import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class TestApp {


    public void generateSimpleTest() {
        MyList myList = MyList.getInstance();
        ArrayList<String> list = new ArrayList<>();
        list.add("Bucuresti");
        list.add("Timisoara");
        myList.addList("Orase", list);
    }

    public void generateSimpleTest2() {
        MyList myList = MyList.getInstance();
        ArrayList<String> list = new ArrayList<>();
        list.add("Romania");
        list.add("Ungaria");
        myList.addList("Tari", list);
    }

    public void generateComplexTest() {
        MyList myList = MyList.getInstance();
        ArrayList<String> list = new ArrayList<>();
        list.add("Salam");
        list.add("Cascaval");
        myList.addList("Mezeluri", list);
    }

    @Test
    public void addSimpleList() {
        MyList myList = MyList.getInstance();
        generateSimpleTest();
        ArrayList<String> namesList = new ArrayList();
        namesList.add("Bucuresti");
        namesList.add("Timisoara");
        Assert.assertEquals(namesList, myList.lists.get("Orase"));
    }

    @Test
    public void SimpleDuplicate() {
        MyList myList = MyList.getInstance();
        generateSimpleTest();
        myList.duplicateList("Orase");
        Assert.assertEquals(myList.lists.get("Orase"), myList.lists.get("Orase (1)"));
    }

    @Test
    public void SimpleDuplicate2() {
        MyList myList = MyList.getInstance();
        generateSimpleTest2();
        myList.duplicateList("Tari");
        myList.duplicateList("Tari (1)");
        Assert.assertEquals(myList.lists.get("Tari"), myList.lists.get("Tari (2)"));
    }

    @Test
    public void ComplexDuplicate() {
        MyList myList = MyList.getInstance();
        generateComplexTest();
        myList.duplicateList("Mezeluri");
        myList.duplicateList("Mezeluri (1)");
        myList.duplicateList("Mezeluri");
        myList.duplicateList("Mezeluri");
        myList.duplicateList("Mezeluri (4)");
        myList.duplicateList("Mezeluri (2)");
        Assert.assertEquals(myList.lists.get("Mezeluri"), myList.lists.get("Mezeluri (6)"));
    }

    @Test
    public void finalDuplicateWithDelete() {
        MyList myList = MyList.getInstance();
        generateComplexTest();
        myList.duplicateList("Mezeluri");
        myList.duplicateList("Mezeluri");
        myList.duplicateList("Mezeluri");
        myList.duplicateList("Mezeluri");
        myList.duplicateList("Mezeluri");
        myList.duplicateList("Mezeluri (1)");
        myList.duplicateList("Mezeluri");
        myList.duplicateList("Mezeluri");
        myList.duplicateList("Mezeluri (4)");
        myList.duplicateList("Mezeluri (2)");
        myList.removeList("Mezeluri (2)");
        myList.removeList("Mezeluri (1)");
        myList.duplicateList("Mezeluri (3)");
        myList.duplicateList("Mezeluri (3)");
        myList.duplicateList("Mezeluri (3)");
        Assert.assertEquals(myList.lists.get("Mezeluri"), myList.lists.get("Mezeluri (2)"));

    }
}
