
import java.util.*;
import java.util.stream.Collectors;

public class MyList {
    private static MyList myList = null;
    public HashMap<String, ArrayList<String>> lists;

    private MyList() {
    }

    public void addList(String listName, ArrayList<String> list) {
        myList.lists.put(listName, list);
    }

    public void removeList(String listName) {
        myList.lists.remove(listName);
    }

    public static MyList getInstance() {
        if (myList == null) {
            myList = new MyList();
            myList.lists = new HashMap<>();
        }
        return myList;
    }

    public void duplicateList(String name) {
        String listName = name;
        if (myList.lists.containsKey(name)) {
            if (matchString(name)) {
                listName = getListName(name);
            }
            createCopy2(listName);
        }
    }

    public boolean matchString(String name) {
        if(name.matches("^.*?\\((\\d+)\\).*$")) {
            return true;
        }
        return false;
    }

    public String getListName(String name) {
        StringTokenizer stringTokenizer = new StringTokenizer(name, " (");
        String listName = stringTokenizer.nextToken();
        return listName.trim();
    }


    public static Integer getCopyNumber(String name) {
        StringTokenizer stringTokenizer = new StringTokenizer(name, "(");
        String token = stringTokenizer.nextToken();
        token = stringTokenizer.nextToken();
        stringTokenizer = new StringTokenizer(token.trim(), ")");
        String number = stringTokenizer.nextToken().trim();
        return Integer.valueOf(number);
    }

    public Integer getAvailableNumber(List<Integer> numberList, Integer max) {
        for (int i = 1; i < max; i++) {
            if (!numberList.contains(i))
                return new Integer(i);
        }
        return new Integer(max+1);
    }

    public List<Integer> getNumberList(List<String> names, String name) {
        return names.stream().filter(s -> s.startsWith(name) && s.contains("("))
                .map(MyList::getCopyNumber).collect(Collectors.toList());
    }

    /*
    Functia createCopy2 este facuta ca
    in cazul in care exista Name, Name(1), Name(5), Name(6)...
    Urmatoarea copie va fi Name(2), pentru ca lipseste
     */
    public void createCopy2(String name) {
        List<String> names = new ArrayList<>(myList.lists.keySet());
        Long count = names.stream().filter(s -> s.startsWith(name)).count();
        if (count > 1) {
            List <Integer> numberList = getNumberList(names, name);
            Integer max = Optional.of(numberList.stream().max((o1, o2) -> o1.compareTo(o2))).get().get();
            Integer newNumberCount = getAvailableNumber(numberList, max);
            myList.addList(name+" ("+newNumberCount+")", myList.lists.get(name));
        }
        else {
            List<String> names2 = new ArrayList<>(myList.lists.keySet());
            Long count2 = names2.stream().filter(s -> s.startsWith(name)).count();
            myList.addList(name+" ("+count2+")", myList.lists.get(name));
        }
    }
}
